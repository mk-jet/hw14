//import Foundation
//import RealmSwift
//
//class FiveDayForecastListPersistance: Object {
//    @objc dynamic var main: FiveDayListMainPersistance?
//    @objc dynamic var dt_txt = ""
//}
//
//class FiveDayListMainPersistance: Object {
//    @objc dynamic var temp = 0.0
//    @objc dynamic var feels_like = 0.0
//}
//
//class ForecastPersistance {
//    static let shared = ForecastPersistance()
//    private let fiveDayForecast = try! Realm()
//    
//    func saveForecast(forecastList: [FDForecastList]) {     // Сохраняем прогноз
//        let newForecast = FiveDayForecastListPersistance()
//        
//        if forecastList.count > 0{
//            /* Удаляем из хранилища старые данные */
//            for forecast in fiveDayForecast.objects(FiveDayForecastListPersistance.self){
//                try! fiveDayForecast.write {
//                    fiveDayForecast.delete(forecast)
//                }
//            }
//            /* Записываем новые данные */
//            for forecast in forecastList {
//                newForecast.main!.feels_like = forecast.main.feels_like
//                newForecast.main!.temp = forecast.main.temp
//                newForecast.dt_txt = forecast.dt_txt
//                try! fiveDayForecast.write {
//                    fiveDayForecast.add(newForecast)
//                }
//            }
//        } else { print("--!!! No data in forecastList") }
//    }
//    
//    func loadForecast() -> [FDForecastList]{
//        let forecastPersistance = fiveDayForecast.objects(FiveDayForecastListPersistance.self)
//        var forecastList: [FDForecastList] = []
//        if forecastPersistance.count > 0{
//            for forecast in forecastPersistance {
//                var newForecast = FDForecastList(from: <#Decoder#>)
//                newForecast.main.temp = forecast.main!.temp
//                newForecast.main.feels_like = forecast.main!.feels_like
//                newForecast.dt_txt = forecast.dt_txt
//                forecastList.append(newForecast)
//            }
//        } else {print("!!!--- No data in persistance")}
//        return forecastList
//    }
//}
