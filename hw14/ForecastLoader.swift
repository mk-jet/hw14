//import Foundation
//import Alamofire
//
////let fiveDayForecastUrlString = /* Ссылка на сервер с информацией о прогнозе погоды*/ "http://api.openweathermap.org/data/2.5/forecast?q=Moscow,RU&units=metric&APPID=505c91a89efce765450586b2b6fe88a9"
//
//
//struct FiveDayForecast: Decodable {
//    var list: [FDForecastList]?
//}
//
//struct FDForecastList: Decodable {
//    var main: FDFListMain
//    var dt_txt: String
//}
//
//struct FDFListMain: Decodable {
//    var temp: Double
//    var feels_like: Double
//}
//
//class ForecastLoader {
//    static let shared = ForecastLoader()
//
//    func loadFiveDayForecast(completion: @escaping ([FDForecastList]) -> Void){
//        AF.request(fiveDayForecastUrlString).responseData { response in
//            if let data = response.value {
//                var fiveDayForecast: [FDForecastList] = []
//                DispatchQueue.main.async {
//                    do {
//                        let forecast = try JSONDecoder().decode(FiveDayForecast.self, from: data)
//                        for eachTime in forecast.list! {
//                            fiveDayForecast.append(eachTime)
//                        }
//                    } catch let error { print(error) }
//                    completion(fiveDayForecast)
//                    print(fiveDayForecast.count)
//                }
//            }
//        }
//    }
//}
