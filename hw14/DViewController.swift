import UIKit

class DViewController: UIViewController {

    @IBOutlet var forecastTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    var forecast: [ForecastList] = []
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        forecastTableView.delegate = self
        forecastTableView.dataSource = self
        forecast = Forecast().getForecast()
        forecastTableView.reloadData()
        Forecast().loadForecast {
            self.forecastTableView.reloadData()
        }
        self.view.layoutIfNeeded()
    }
}

extension DViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return forecast.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DTableViewCell", for: indexPath) as! DTableViewCell
        cell.dateAndTimeLabel.text = forecast[indexPath.row].dt_txt
        cell.currentTempLabel.text = "\(String(describing: forecast[indexPath.row].temp))˙C"
        cell.feelsLikeTempLabel.text = "Feels like \(String(describing: forecast[indexPath.row].feels_like))˙C"
        return cell
    }
}
