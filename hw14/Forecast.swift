import Foundation
import RealmSwift
import Alamofire

class ForecastList: Object {
    @objc dynamic var dt_txt = ""
    @objc dynamic var temp = 0.0
    @objc dynamic var feels_like = 0.0
}

class Forecast {
    private let forecastRealm: Realm
    private let forecastList: Results<ForecastList>

    
    init() {
        forecastRealm = try! Realm()
        forecastList = forecastRealm.objects(ForecastList.self)
    }
    
    public func getForecast() -> [ForecastList] {
        var forecastToShare = [ForecastList()]
        if forecastList.count > 0 {
            for forecast in forecastList {
                let timeForecast = ForecastList()
                timeForecast.dt_txt = forecast.dt_txt
                timeForecast.temp = forecast.temp
                timeForecast.feels_like = forecast.feels_like
                forecastToShare.append(timeForecast)
            }
        }
        print (self.forecastList)
        return forecastToShare
    }
    
    public func loadForecast(completion: @escaping () -> Void) {
        let forecastUrl = "http://api.openweathermap.org/data/2.5/forecast?q=Moscow,RU&units=metric&APPID=505c91a89efce765450586b2b6fe88a9"
        AF.request(forecastUrl).responseJSON(completionHandler: { [self] response in
            if let data = response.value as? [String : Any],
                let jsonDict = data["list"] as? [NSDictionary]{
                try! forecastRealm.write({ forecastRealm.delete(forecastList) })
                for each in jsonDict{
                    let list = ForecastList()
                    if let dateNTime = each["dt_txt"] as? String,
                        let tempData = each["main"] as? [String: Double]{
                        list.dt_txt = dateNTime
                        list.temp = tempData["temp"] ?? 0
                        list.feels_like = tempData["feels_like"] ?? 0
                        try! forecastRealm.write({
                            forecastRealm.add(list)
                        })
                    }
                    DispatchQueue.main.async {
                        completion()
                    }
                }
            }
        })
        
    }
}
